# MAC0110 - MiniEP7
# Gustavo Nogai Saito - 10730021

function sin(x)
    t = 0 
    resp = 0
    a = 0
    while t <= 10
        a = ((-1) ^ t) * (x ^ (2 * t + 1)) / factorial(big(2 * t + 1))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function cos(x)
    t = 0
    resp = 0
    a = 0
    while t <= 10
        a = ((-1) ^ t) * (x ^ (2 * t)) / factorial(big(2 * t))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end
    
function tan(x)
    t = 1
    resp = 0
    a = 0
    while t <= 11
        a = ((2 ^ (2 * t)) * ((2 ^ (2 * t)) - 1) * convert(BigFloat, bernoulli(t)) * (x ^ ((2 * t) - 1))) / factorial(big(2 * t))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function quaseigual(v1, v2)
    erro = 0.0001
    igual = abs(v1 - v2)
    if igual <= erro
        return true
    else
        return false
    end
end

function check_sin(value, x)
    return quaseigual(value, sin(x))
end

function check_cos(value, x)
    return quaseigual(value, cos(x))
end

function check_tan(value, x)
    return quaseigual(value, tan(x))
end

function taylor_sin(x)
    t = 0 
    resp = 0
    a = 0
    while t <= 10
        a = ((-1) ^ t) * (x ^ (2 * t + 1)) / factorial(big(2 * t + 1))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function taylor_cos(x)
    t = 0
    resp = 0
    a = 0
    while t <= 10
        a = ((-1) ^ t) * (x ^ (2 * t)) / factorial(big(2 * t))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end
    
function taylor_tan(x)
    t = 1
    resp = 0
    a = 0
    while t <= 11
        a = ((2 ^ (2 * t)) * ((2 ^ (2 * t)) - 1) * convert(BigFloat, bernoulli(t)) * (x ^ ((2 * t) - 1))) / factorial(big(2 * t))
        resp += a
        t += 1
    end
    resp += a
    return resp
end

function test()
    if !quaseigual(sin(pi / 6), 0.5)
        println("Erro para sin(pi / 6)")
    end
    if quaseigual(sin(pi), 42)
        println("Erro para sin(pi)")
    end
    if !quaseigual(cos(pi / 3), 0.5)
        println("Erro para cos(pi / 3)")
    end
    if quaseigual(cos(pi), 42)
        println("Erro para cos(pi)")
    end
    if !quaseigual(tan(pi / 6), 0.5773)
        println("Erro para tan(pi / 6)")
    end
    if quaseigual(tan(pi / 4), 42)
        println("Erro para tan(pi / 4)")
    end
    if !check_sin(0.5, pi / 6)
        println("Erro para check_sin(0.5, pi / 6)")
    end
    if !check_cos(0.5, pi / 3)
        println("Erro para check_cos(0.5, pi / 3")
    end
    if !check_tan(0.5773, pi / 6)
        println("Erro para check_tan(0.5773, pi / 6)")
    end
    println("Final dos Testes")
end